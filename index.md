---
layout: blocks
lang: en
title: home
date: 2020-11-20T22:00:00.000+00:00
page_sections:
- template: HERO_block
  block: HERO_block
  teaser-title: No internet? Catch Second Wind.
  teaser-paragraph: Second Wind is an offline distribution system for Android apps
    and more! Check out our carefully curated collection of apps optimized for offline
    scenarios.
  teaser-img: "/assets/img/teaser.png"
  teaser-button-url: /en/repo/
  teaser-button-text: Get Started
  bg-color: ''
  text-color: ''
- template: SHORT_INTRO_block
  block: SHORT_INTRO_block
  row: normal
  bg-color: ''
  text-color: black
  img: "/assets/img/off-grid.png"
  title: When off-grid.
  paragraph: Find your way with offline maps.<br/><a href="/en/repo">Get the Second Wind Repo</a> to discover more apps.
  column:
  - img: "/assets/img/osmand.png"
    title: OsmAnd~
    text: Mobile maps and navigation
  - img: "/assets/img/trail.png"
    title: Trail Sense
    text: Use your phone’s sensors to assist with wilderness treks or survival situations
  - img: "/assets/img/stop.png"
    title: Stop-o-Moto
    text: Use pictures to make gif and video files
- template: SHORT_INTRO_block
  block: SHORT_INTRO_block
  row: revers
  bg-color: "#3078EF"
  text-color: white
  img: "/assets/img/during-out.png"
  title: During outages.
  paragraph: Survival guides, maps and secure messaging make your safety a priority.<br/><a href="/en/repo">Get the Second Wind Repo</a> to discover more apps.
  column:
  - img: "/assets/img/umbrella-logo.png"
    title: Umbrella
    text: Training guides, first aid and advice for surviving
  - img: "/assets/img/brair.png"
    title: Briar
    text: Secure messaging, anywhere, anytime
  - img: "/assets/img/maps.png"
    title: Maps
    text: Offline maps
- template: SHORT_INTRO_block
  block: SHORT_INTRO_block
  row: normal
  img: "/assets/img/during-disaster.png"
  title: During disaster.
  paragraph: Capture the scene, find solace or record important information. <a href="/en/repo">Get the Second Wind Repo</a> to discover more apps.
  column:
  - img: "/assets/img/medic.png"
    title: Medic Log
    text: Log important medical information
  - img: "/assets/img/proff.png"
    title: ProofMode
    text: Turn your photos and videos into secure, signed visual evidence
  - img: "/assets/img/moice.png"
    title: Noice
    text: Put on some background noise and help minimise stress while boosting productivity
  bg-color: ''
  text-color: ''
- template: SHORT_INTRO_block
  block: SHORT_INTRO_block
  row: revers
  bg-color: "#F5D6CA"
  img: "/assets/img/just.png"
  title: Just for fun.
  paragraph: Classic games and star charting offer hours of entertainment. <a href="/en/repo">Get the Second Wind Repo</a> to discover more apps.
  column:
  - img: "/assets/img/crosswords.png"
    title: CrossWords
    text: A scrabble-like game
  - img: "/assets/img/planisphere.png"
    title: Planisphere
    text: Chart the stars, planets, and constellations
  - img: "/assets/img/mindsweeper-and-solitaire.png"
    title: MineSweeper and Solitare
    text: Classic games, endless fun
  text-color: ''
- template: THRESS_OPTIONS_block
  block: THRESS_OPTIONS_block
  go-head-title: GO AHEAD
  go-head-sub-title: Try Second Wind.
  go-head-list:
  - img-group:
    - img: "/assets/img/ic_flexible.svg"
    title: 'It’s Flexible. '
    text: Choose the distribution method that works for you! The only hardware required
      are Android phones.
  - img-group:
    - img: "/assets/img/ic_scalable.svg"
    title: It’s Scalable.
    text: 'Share with multiple people at once. You can even build your own mesh network. '
  - img-group:
    - img: "/assets/img/ic_free.svg"
    title: It’s Free.
    text: The software is free and open source. No adds and no pesky data tracking
      by 3rd parties.
- template: BRANDS_block
  block: BRANDS_block
  by-title: BROUGHT TO YOU BY
  by-list:
  - img: "/assets/img/logo-gp.png"
    url: https://guardianproject.info/
  - img: "/assets/img/logo-okt.png"
    url: https://okthanks.com/
  - img: "/assets/img/logo-fdroid.png"
    url: http://fdroid.org/
- template: CTA_block
  block: CTA_block
  about-title: Catch Second Wind.
  about-paragraph: 'A collection of apps tuned to work offline and a fairly easy way to share them.'
  about-btn-url: /en/repo/
  about-btn-text: Get Started
  by-title: BROUGHT TO YOU BY

---
