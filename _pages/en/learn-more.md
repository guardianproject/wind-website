---
layout: blocks
lang: en
title: Learn More
permalink: "/en/learn-more"
page_sections:
- template: HERO_GET_STARTED_block
  block: HERO_GET_STARTED_block
  teaser-2-title: Learn More
- template: THRESS_OPTIONS_block
  block: THRESS_OPTIONS_block
  go-head-bg: white
  go-head-title: GET SETUP BEFORE GOING OFFLINE
  go-head-sub-title: Choose How to Share
  go-head-list:
  - img-group:
    - img: "/assets/img/ic_nearby-2x.png"
    title: F-Droid Nearby
    text: <a href="https://f-droid.org/" target="_blank">F-Droid</a> Nearby is great
      for one to one sharing
  - img-group:
    - img: "/assets/img/ic_hotspot-2x.png"
    title: Jump Drives
    text: Jump drives are a great way to share with groups of people.
  - title: Hotspot or Mesh Network
    text: A hotspot makes it easy for many to connect to the offline store.
    img-group:
    - img: "/assets/img/ic_jump-drive-2x.png"
- template: VIDEO_block
  block: VIDEO_block
  video: https://www.youtube.com/embed/D5mJshM6CLo
  video-img: "/assets/img/vid01-nearby.png"
  video-sub-title: TUTORIAL
  video-title: F-Droid Nearby
  video-accordion:
  - title: 'Get ready. '
    text: 'This method is great for 1-to-1 sharing. You will need Android phones.
      If you don’t already have F-Droid downloaded on 1 device, you will need the
      internet to begin this process. '
  - title: Download F-Droid.
    text: 'Download F-Droid, an independent app store for Android. Go to the <a href="https://f-droid.org/"
      target="_blank">F-Droid website</a> for a direct download. '
  - title: Add the Second Wind repo to F-Droid.
    text: 'You can get the Second Wind repo by scanning the <a href="https://secondwind.guardianproject.info/repo"
      target="_blank">Second Wind QR Code</a>  or manually adding it by copy/pasting
      the repo link into F-Droid. </br> </br> If you want to get it using the QR code,
      either scan it using the camera from another device, or take a screenshot of
      it and use a QR scanner to scan it from the same device. </br> </br>To manually
      add the repo, copy the <a href="https://guardianproject-wind.s3.amazonaws.com/fdroid/repo?fingerprint=182CF464D219D340DA443C62155198E399FEC1BC4379309B775DD9FC97ED97E1"
      target="_blank">Wind Repo Link</a>. </br> </br>Then open F-droid. Go to settings.
      Select repositories. Go to the “+” on the top right and paste the link there.
      Select ‘add’. The Wind Repo will appear on the Repositories list in your device.   '
  - title: Share F-Droid with your peer.
    text: 'Before sharing apps with a peer, they will need to have F-Droid on their
      phone. They can download F-Droid directly from the website or, if you’re offline,
      you can share the app via bluetooth or by scanning a QR code. To share via bluetooth,
      make sure you both set your devices to be discoverable within the F-Droid app
      by toggling on either bluetooth or wifi. Then in F-Droid, tap ‘Nearby’. Select
      ‘Find people near me’. Select your friend’s device then tap ‘Send F-Droid’. '
  - title: Find your peer on F-Droid.
    text: After you both have F-Droid you need to find one another within the app.
      To do this both open F-Droid and tap ‘Nearby’. Tap ‘Find people near me’. Then
      select your peer’s device. If you or your peer doesn’t show up in the list,
      try scanning their QR code. There’s an option to scan it at the bottom of the
      screen. For screenshots and step-by-step views of this part of the process,
      check out the Nearby tutorial on the <a href="https://f-droid.org/en/tutorials/swap/"
      target="_blank">F-Droid website</a>.
  - title: Select apps to share.
    text: 'Once you are connected to your peer in F-Droid Nearby, you will see a list
      of the apps on your phone. Select which ones to share. Tap ‘Next’ (-->) and
      the apps chosen will show up on your peer’s phone. They can now tap to select
      which apps to install. '
  - title: Celebrate.
    text: You did it! Give your peer a high five! If you experience any difficulties,
      don’t worry. Many can be resolved by quitting the app and restarting.
- template: VIDEO_block
  block: VIDEO_block
  video: https://www.youtube.com/embed/g3BgElh2EHg
  video-img: "/assets/img/vid02-jump.png"
  video-sub-title: TUTORIAL
  video-title: Jump Drives
  video-accordion:
  - title: Get ready.
    text: 'This method is great for sharing Wind on the go! You will need a 2 GB+
      jump drive  that fits into your Android phone or has an adapter compatible with
      your Android phone. You need a desktop computer and internet to begin this process. '
  - title: Troubleshooting Note
    text: Some jump drives will require you to install their own driver for your phone
      to be able to read them. Others might not work with the adapter that comes with
      the jump drive, but they might work with the USB C adapter that comes with some
      phones. We found that even the devices that can connect to your phone without
      an adapter might not work using the integrated adapter, but they do work if
      you use the adapter from your phone.
  - title: Load the jump drive.
    text: On your computer, create a portable version of the Wind repo by downloading
      the <a href="https://guardianproject-wind.s3.amazonaws.com/repo.zip"
      target="_blank">WIND REPO ZIP FILE</a>. Then save it unzipped onto your jump drive. The
      Wind repo contains a curated set of offline apps, along with 3 map files—1 for
      Puerto Rico and two for the Virgin Islands.
  - title: Install F-Droid on the Android phone you want receiving the apps.
    text: 'To be able to receive apps from the jump drive, the phone needs to have
      F-Droid installed. Download F-Droid, an independent app store for Android. Go
      to the <a href="https://f-droid.org/" target="_blank">F-Droid website</a> for
      a direct download or QR code. Please note that you need the internet for this
      step.  '
  - title: Insert the jump drive into an Android phone and open F-Droid.
    text: Once you have the jump drive loaded with the Wind repo and the phone has
      F-Droid installed, plug the jump drive into your phone and open F-Droid. Go
      to the “Nearby tab”. The “USB OTG” ( On The Go) feature will be activated, select
      it. A pop-up window will ask you if you wish to add the new repository. Select
      “ADD MIRROR” to add the Wind repo to your phone.
  - title: View the apps in F-Droid.
    text: 'In F-Droid, go back to the main screen and open the “Categories” tab. Scroll
      down to the “Offline” category and you should see all the Wind apps. The apps
      are available, but do not automatically install. Select the ones you want and
      install them on your device. '
  - title: Celebrate.
    text: 'Congrats! You are locked and loaded to share on the go! If you experience
      any difficulties, don’t worry. Many can be resolved by quitting the app and
      restarting. '
- template: VIDEO_block
  block: VIDEO_block
  video-sub-title: TUTORIAL
  video-title: Share Via Hotspot
  video-accordion:
  - title: Get ready.
    text: 'This method is great for sharing Wind with your community. You will need
      a Raspberry Pi, a desktop computer, the internet, and some coding skills. '
  - title: Setup your Raspberry Pi.
    text: 'There are multiple ways to do this. Check out our <a href="https://gitlab.com/guardianproject/wind-offline-fdroid-repo-on-rpi"
      target="_blank">detailed documentation on Gitlab</a> for full instructions.
      Interested in having someone set it up for you? Check out <a href="https://guardianproject.info/contact/"
      target="_blank">Guardian Project</a> to get in touch. '
  - title: Publicize for your community.
    text: 'Once your device is completely set up with the Wind repo, tell your community
      about it. Share the URL or post a QR code. People can easily download apps by
      going to the URL or scanning the QR code. Have them download F-Droid first.
      Then they can get the apps they want.  Don’t stop with just the Wind repo! Raspberry
      pi’s can be loaded with all sorts of content to share like videos, educational
      materials, resources, songs and more! '
  video-img: "/assets/img/vid03-pi.png"
  video: https://www.youtube.com/embed/qMT-0mz1lkI
- template: CTA_block
  block: CTA_block
  about-title: Need help?
  about-paragraph: Contact Guardian Project with questions or to learn about getting
    pre-configured devices.
  about-btn-text: Reach Out
  about-btn-url: https://guardianproject.info/contact/
date: 

---
