---
layout: blocks
lang: es
title: Obten las Apps
permalink: "/es/repo"
date: 2021-02-08T05:00:00.000+00:00
page_sections:
- template: THRESS_OPTIONS_block
  block: THRESS_OPTIONS_block
  go-head-title: OBTENER LAS APLICACIONES
  go-head-sub-title: Instala F-Droid y agrega el Repositorio de Segundo Viento para ver nuestra seleccion de Aplicaciones
  go-head-list:
  - img-group:
    - img: "/assets/img/f-droid-download-qr.png"
    title: Instala F-Droid
    text: 'Para obtener la seleccion de aplicaciones de Segundo Viento, primero instala <a href="https://f-droid.org" target="_blank">F-Droid</a>.<br/> Escanea el Scan the Código QR de arriba, descárgualo del sitio web de F-Droid, o consígelo de un amigo si no estás conectado. Para obtener ayuda con esto, ve nuestro video: <a href="https://www.youtube.com/watch?v=o-kqQQqb-Sw&list=PL4-CVUWabKWdOPRjWwAFn-26pSEml303U&index=5"
      target="_blank">Install F-Droid.</a>'
  - img-group:
    - img: "/assets/img/second-wind-download-qr.png"
    title: Agrega Segundo Viento
    text: 'Escanee el código QR de arriba para agregar el repositorio a tu aplicación F-Droid. También puedes agrégalo abriendo este enlace: <a href="https://guardianproject-wind.s3.amazonaws.com/fdroid/repo?fingerprint=182CF464D219D340DA443C62155198E399FEC1BC4379309B775DD9FC97ED97E1" target="_blank">Apps de Segundo Viento.</a> Asegurate de tener F-Droid instalado primero.  Si necesitas ayuda con esto, ve nuestro video: <a href="https://www.youtube.com/watch?v=y3bjUsd3Qoc&list=PL4-CVUWabKWdOPRjWwAFn-26pSEml303U" target="_blank"> Compartir un repositorio de F-Droid usando un código QR.</a>'
  - img-group:
    - img: "/assets/img/second-wind-download-qr-1.png"
    title: Encuentra las Apps
    text: Una vez que hayas agregado el repositorio Segundo Viento, puedes encontrar la selección de aplicaciones en la vista 'Categorías' de F-Droid. Si no las ves, ve a 'Configuración' y desactiva todos los demás repositorios. Este cambio te permitirá ver solo las aplicaciones de Segundo Viento.

---
