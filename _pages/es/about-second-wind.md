---
layout: blocks
lang: es
title: Acerca de Seegundo Viento
permalink: "/es/about"
date: 2021-02-08T05:00:00.000+00:00
page_sections:
- template: HERO_GET_STARTED_block
  block: HERO_GET_STARTED_block
  teaser-2-title: Acerca de Segundo Viento
- template: HERO_block
  block: HERO_block
  teaser-paragraph: En un momento repentino, la conexión que tenemos entre nosotros y la información
     se puede alterar o desaparecer por completo. Hemos visto esto pasar en los peores  momentos
     cuando desastres naturales ocurren y el acceso a la informacion es mas necesarion que nunca. <br> <br>
      Cuando el huracán María golpeó a Puerto Rico y las áreas circundantes en 2017, más de un millón de personas
     perdio acceso a internet y teléfonia celular. Es en momentos como ese, donde la información puede
     salvar tu vida, guiándote hacia lugares seguros y los servicios. <br> <br> 
     Las redes de hoy son frágiles y se sobrecargan fácilmente. No ha habido suficiente inversión
     en redes alternativas. Hay mucho potencial en las radios, sensores y
     procesamiento disponible en el más barato de los teléfonos inteligentes y enrutadores para conectar a las personas
     cuando el Internet global no es una opción. <br> <br> En <a href="https://guardianproject.info">Guardian Project</a>, hemos
     estado trabajando para explotar este potencial y lanzar tecnología de código abierto que comienza
     con el teléfono inteligente en la mano. Segundo Viento es el comienzo.  <br> <br> Desde
     2012, hemos trabajado para desarrollar capacidades para compartir sin conexión a través de F-Droid, una
     tienda de aplicaciones independiente y de código abierto. Las aplicaciones en el repositorio Segundo Viento para     
     <a href="https://f-droid.org">F-Droid</a> ofrecen una variedad de funciones, desde navegación, distribución de información, documentación
     y bienestar. Conmovidos por la situación en Puerto Rico, hemos incluido Open
     Street Maps (OSM) para equipar a las personas con los mapas que funcionan sin conexion a Internet. 
     El repositorio de aplicaciones incluye archivos de mapas para Puerto Rico y las Islas Vírgenes. Aprende más
    sobre la gestión de mapas sin conexión con <a href="https://f-droid.org">F-Droid</a> y <a href="https://osmand.net/">OsmAnd</a>. <br> <br> <b> Dando un paso atrás
    </b> <br> <br> Segundo Viento es el hijo de "Wind", un concepto y un nombre en clave que
    representa el pensamiento holístico y las posibilidades que estamos explorando asociados
    con capacidades tecnológicas asequibles y sin conexión a la red. <br> <br> "Viento", como concepto,
    es una red diseñada para la comunicación oportunista y el intercambio de conocimientos locales.
    Se basa en la impermanencia, el movimiento y la espontaneidad. El viento es un contrapunto directo
    a la metáfora de la Web, un sistema construido sobre el concepto de física fija
    nodos, autoridades centralizadas y enlaces permanentes. Tiene sus raíces en la mentalidad
    y necesidades de las personas y comunidades que enfrentan desafíos para comunicarse. El viento es
    moldeado por los movimientos y la densidad de las personas en el tiempo y el espacio. Explora en el blog
    publicaciones y otras ideas en <a href="https://guardianproject.info/code/wind">sitio web del proyecto</a>. <br> <br> <b> Un especial
    agradecimiento </b> <br> <br> Queremos extender un agradecimiento especial a Mozilla y al
    Fundación Nacional de Ciencias por apoyar este trabajo bajo la <a href="https://guardianproject.info/2018/09/26/wind-is-a-mozilla-national-science-foundation-grand-prize-winner/">Innovación Inalámbrica para Networked Society (WINS) Challenge</a>.
  bg-color: "#FFFFFF"
  text-color: ''
  teaser-button-text: Conoce a nuestro equipo
  teaser-button-url: https://guardianproject.info/team/
  teaser-img: "/assets/img/white-bck.png"
  teaser-title: Brindándote una nueva fuerza para conectarte
---
