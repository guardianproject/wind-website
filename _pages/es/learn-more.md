---
layout: blocks
lang: es 
title: Aprende Más
permalink: "/es/aprende-mas/"
page_sections:
- template: HERO_GET_STARTED_block
  block: HERO_GET_STARTED_block
  teaser-2-title: Aprende Más
- template: SHORT_INTRO_block
  block: SHORT_INTRO_block
  img: "/assets/img/fdroid-wind-repoqr.png"
  title: 'Repositorio Segundo Viento '
  paragraph: Escanea este codigo QR con la aplicacion de Camara para descargar el Repositorio 
    Segundo Viento de F-Droid y empieza a compartir con otros. 
- template: THRESS_OPTIONS_block
  block: THRESS_OPTIONS_block
  go-head-bg: white
  go-head-title: CONFIGURA TU REPOSITORIO CUANDO TENGAS ACCESO A INTERNET
  go-head-sub-title: Elige como compartir
  go-head-list:
  - img-group:
    - img: "/assets/img/ic_nearby-2x.png"
    title: F-Droid Cercano
    text: <a href="https://f-droid.org/" target="_blank">F-Droid</a> Cercano es la mejor opcion
     para compartir uno a uno 
  - img-group:
    - img: "/assets/img/ic_hotspot-2x.png"
    title: Repositorios Portables
    text: Repositorios portables en USBs son la mejor opcion para compartir en grupos de personas.
  - title: Punto de Acceso o Red Local
    text: Un Punto de Acceso facilita que muchos usuarios se conecten a la tienda de applicaciones fuera de linea. 
    img-group:
    - img: "/assets/img/ic_jump-drive-2x.png"
- template: VIDEO_block
  block: VIDEO_block
  video: https://www.youtube.com/embed/gg6HByuR6nk
  video-img: "/assets/img/vid01-nearby.png"
  video-sub-title: TUTORIAL
  video-title: F-Droid Cercano
  video-accordion:
  - title: 'Preparate '
    text: 'Este metodo es la mejor opcion para compartir uno a uno. Necesitaras telefonos Android. 
      Si no tienes F-droid instalado al menos en un dispositivo necesitaras Internet
      para empezar este proceso. '
  - title: Descarga F-Droid.
    text: 'Descarga F-Droid, una aplicacion de tienda de apps independiente para Android. Ve a <a href="https://f-droid.org/"
      target="_blank">F-Droid website</a> para una descarga directa.'
  - title: Agrega el repositorio Segundo Viento a F-Droid.
    text: 'Puedes obtener el repositorio Segundo Viento escaneando el codigo QR en la parte de arriba de esta pagina <img src="/assets/img/fdroid-wind-repoqr.png" alt="Codigo QR de el Repositorio Segundo Viento"> <br/> <br/> o agregarlo manualmente copiando y pegando el link en F-Droid. <br/> </br> Si lo quieres obtener usando el codigo QR escanealo con la camara o toma una captura de pantalla y usa el escaner QR para escanearlo desde el mismo dispositivo. </br> </br>Para agregar el repositorio manualmente, copia el <a href="https://guardianproject-wind.s3.amazonaws.com/fdroid/repo?fingerprint=182CF464D219D340DA443C62155198E399FEC1BC4379309B775DD9FC97ED97E1" target="_blank">Wind Repo Link</a>. </br> </br> Abre F-Droid. Ve a configuracion.  Selecciona Repositorios. Ve a el signo “+” arriba a la derecha y pega el lik ahi.  Selecciona ‘agregar’. El repositorio Segundo Viento aparecera en la lista de Repositorios de tu dispositivo.'
  - title: Comparte F-Droid con alguien cercano. 
    text: 'Antes de compartir aplicaciones con alguien cercano necesitan tener instalado F-Droid en su telefono.
      Pueden descargar F-Droid directamente de el sitio web o si no tienen internet puedes compartir la aplicacion 
      via bluetooth o escanear el codigo QR. Para compartir via bluetooth,
      asegurate que ambos dispositivos esten en modo visible en F-Droid activando la opcion Wifi o Bluetooth.
      Despues en F-Droid, selecciona ‘Cercano’. Selecciona
      ‘Encontrar personas cercanas’. Selecciona el dispositivo de tu amigo y selecciona Enviar F-Droid. '
  - title: Encuentra a alguien cercano en F-Droid.
    text: Despues de que ambos tienen F-Droid instalado necesitan encontrarse el uno al otro en la aplicacion. 
      Para hacer esto ambos deben abrir F-Droid y seleccionar Cercano. Selecciona encontrar personas cerca de mi. Luego selecciona el dispositivo de tu amigo. Si el dispositivo de tu amigo o el tuyo no aparecen en la lista trata escaneando 
      su codigo QR. Hay una opcion para escanear el codigo en la parte de abajode la pantalla.
    
  - title: Selecciona aplicaciones para compartir.
    text: 'Una vez conectado a tu amigo en F-Droid Cercano,veras una lista de las  
      aplicaciones en tu telefono. Selecciona las que deseas compartir. Selecciona ‘Siguiente’ (-->) y
      las aplicaciones seleccionadas apareceran en el telefono de tu amigo. Ahora el puede seleccionar 
      las que deseea instalar. Para capturas de pantalla y un tutorial paso a paso visita el tutorial
     en el  <a href="https://f-droid.org/en/tutorials/swap/"
      target="_blank"> sitio web de F-Droid </a>.'
  - title: Celebra!
    text: Lo lograste! Si tienes algun problema, no te preocupes,  generalmente puedes resolverlo cerrando la aplicacion y reiniciando tu telefono. 
- template: VIDEO_block
  block: VIDEO_block
  video: https://www.youtube.com/embed/9xQ2l42sRxo
  video-img: "/assets/img/vid02-jump.png"
  video-sub-title: TUTORIAL
  video-title: Repositorio portátil de USB
  video-accordion:
  - title: Preparate.
    text: 'Este metodo es el mas recomendable para compartir las aplicaciones de Segundo Viento cuando tienes prisa.Necesitars una memoria USB de 2 GB o mas.
      USB compatible con dispositivos Android, una computadora y acceso a internet 
      para iniciar el proceso. '
  - title: Nota de configuracion
    text: Algunos dispositivos USB necesitaran que instales su propio driver en tu telefono para poder leerlos. Otros no funcionaran con el adaptador integrado pero puede que funcionen con el adaptador que viene con tu dispositivo
      
  - title: Carga el dispositivo.
    text: 'En tu computadora, crea una version portatil de el repositorio Segundo Viento, create a portable descargando el <a href="https://www.google.com/url?q=https://guardianproject-wind.s3.amazonaws.com/repo.zip" target="_blank">REPO ZIP FILE</a>. Luego guarda el archivo descomprimido en tu USB.  El repositorio Segundo Viento contiene una seleccion de aplicaciones que no requieren internet para funcionar, ademas de 3 archivos de mapas para Puerto Rico y las Islas Virgenes.'
  - title: Installa F-Droid en el telefono Android que deseas que reciba las aplicciones.
    text: 'Para que puedas recibir las aplicaciones de el USB, el telefono necesita tener instalado F-Droid. 
      Descarga F-Droid, una aplicacion de tienda de apps independiente para Android.Ve
      a <a href="https://f-droid.org/" target="_blank"> el Sitio web de F-Droid </a> para una descarga directa
      o puedes utilizar el codigo QR. Necesitas internet para este paso.  '
  - title: Inserta el USB en el telefono Android para el siguiente paso.
    text: 'Una vez que tienes el USB cargado con el  repositorio Segundo Viento y el telefono tiene F-Droid Instalado,
      conecta el USB en el telefono y abre F-Droid. Selecciona Cercano. La opcion "USB OTG" sera activada
      seleccionala. Se abrira una ventana  que te preguntara si deseas agregar el repositorio. 
      selecciona "AGREGAR ESPEJO" para agregar el repositorio a tu telefono'
  - title: Ver las aplicaciones en F-Droid.
    text: 'En F-Droid, regresa a la pantalla principal abre la pestana de “Categorias”. Ve abajo a la categoria “No Internet” y todas las aplicaciones de Segundo Viento deberan aparecer ahi. Las aplicaciones estan disponibles pero no instaladas. Selecciona las que deseas installar.'
  - title: Celebra.
    text: 'Felicitaciones! Estas listo para compartir las apps!
     Si se presentan problemas durante el proceso reinicia tu telefono e intenta de nuevo. '
- template: VIDEO_block
  block: VIDEO_block
  video-sub-title: TUTORIAL
  video-title: Comparte via Punto de Acceso
  video-accordion:
  - title: Preparate.
    text: ' Este metodo es el mejor para compartir Segundo Viento con una comunidad grande. Necesitaras
      un Raspberry Pi, una computadora, internet, y experiencia en programación. '
  - title: Prepara tu Raspberry Pi.
    text: 'Hay muchas maneras de hacer esto. Ve a <a href="https://gitlab.com/guardianproject/wind-offline-fdroid-repo-on-rpi"
      target="_blank">para documentacion detallada en Gitlab</a> para instrucciones completas.
      ¿Te interesa que alguien te ayude con la configuracion?  ve a <a href="https://guardianproject.info/contact/"
      target="_blank">Guardian Project</a> to get in touch. '
  - title: Publica para tu comunidad.
    text: 'Una vez que tu Punto de Acceso esta configurado, informa a tu comunidad, comparte el link o el codigo QR.
      Ellos pueden descargar las aplicaciones facilmente abriendo el link o escaneando el codigo QR.
      Pero, deben descargar F-Droid primero.'
  video-img: "/assets/img/vid03-pi.png"
  video: https://www.youtube.com/embed/qMT-0mz1lkI
- template: CTA_block
  block: CTA_block
  about-title: Necesitas Ayuda?
  about-paragraph: Si tienes preguntas contactanos o informate de como obtener dispositivos pre-configurados.
  about-btn-text: Contacto
  about-btn-url: https://guardianproject.info/contact/
date: 

---
