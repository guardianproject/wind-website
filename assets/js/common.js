 
$('.menu-button').click(function() {
  $('.menu-button').toggleClass('active')
  $('.header_cont_nav_list').toggleClass('active')
})

$(window).scroll(function() {
  var scroll = $(window).scrollTop()
  if (scroll >= 50) {
    $('.header').addClass('active')
  } else {
    $('.header').removeClass('active')
  }
})

$('.header_cont_nav_list_items').click(function() {
  if (!$(this).hasClass('active')) {
    $('.header_cont_nav_list_items').removeClass('active')
  }
  $(this).toggleClass('active')
})


$('.accordion_item_title').toggleClass('inactive-header');
var contentwidth = $('.accordion_item_title').width();
$('.accordion_item_content').css({'width' : contentwidth });
  
$('.accordion_item_title').click(function () {
  if($(this).is('.inactive-header')) {
    $('.active-header').toggleClass('active-header').toggleClass('inactive-header').next().slideToggle().toggleClass('open-content');
    $(this).toggleClass('active-header').toggleClass('inactive-header');
    $(this).next().slideToggle().toggleClass('open-content');
  } 
  else {
    $(this).toggleClass('active-header').toggleClass('inactive-header');
    $(this).next().slideToggle().toggleClass('open-content');
  }
}); 
 
 

$('.video_block .video_play').click(function() {
  let frame = $(this).offsetParent().data("video");
  $(this).offsetParent().find('.video_block_modal').addClass('active');
  // $(this).offsetParent().find('.video_block_modal').find('.video_block_modal_close').click(function() {
  //   $(this).offsetParent().offsetParent().removeClass('active');
  //   $(this).offsetParent().find('iframe').remove();
  // }); 
  let createIframe = document.createElement("iframe");
  createIframe.setAttribute("frameborder", "0");
  createIframe.setAttribute("allow", "autoplay; fullscreen");
  createIframe.setAttribute("allowfullscreen", "");
  createIframe.setAttribute("src", frame);
  $(this).offsetParent().append(createIframe); 
  console.log($(this).offsetParent(), createIframe)
});

 
// $('.video_block_modal_close').click(function() {
//   $('.video_block_modal').removeClass('active');
//   $( ".video_block_modal iframe" ).remove();
// });
 
 
